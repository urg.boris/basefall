// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Components/AudioComponent.h"
#include "Hammer.generated.h"



UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BASEFALL_API UHammer : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHammer();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	float LagrangePolynomial(float x);
	void OnGroundHit();

	FVector startPos;
	float runningTime;
	UAudioComponent* hitAudio;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditAnywhere)
	FVector middlePos;

	UPROPERTY(EditAnywhere)
	FVector endPos;

	UPROPERTY(EditAnywhere)
		class USoundBase* hitSound;

	UPROPERTY(EditAnywhere)
		float hitClosingDist;

	
};
