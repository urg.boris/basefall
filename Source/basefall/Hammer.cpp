﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Hammer.h"


// Sets default values for this component's properties
UHammer::UHammer()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

}


// Called when the game starts
void UHammer::BeginPlay()
{
	Super::BeginPlay();
	startPos = GetOwner()->GetActorLocation();

	if (GEngine) {
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("debug message!"));
		//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, startPos.ToString());
	}
	runningTime = 0;
	hitAudio = nullptr;
	
}



float UHammer::LagrangePolynomial(float x)
{
	float x1 = startPos.X;
	float y1 = startPos.Z;
	float x2 = middlePos.X;
	float y2 = middlePos.Z;
	float x3 = endPos.X;
	float y3 = endPos.Z;

	float y = y1*(x-x2)*(x-x3)/((x1-x2)*(x1-x3)) + y2*(x-x1)*(x-x3)/((x2-x1)*(x2-x3)) + y3*(x-x1)*(x-x2)/((x3-x1)*(x3-x2));
	return y;
}

void UHammer::OnGroundHit()
{
	if (!hitAudio) {
		hitAudio = UGameplayStatics::SpawnSoundAtLocation(this, hitSound, GetOwner()->GetActorLocation(), FRotator::ZeroRotator, 1, 1, 0.3f);
	}
	
	
	//audio->Is
}


// Called every frame
void UHammer::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	//FVector newPos = endPos - startPos;
	float t = 1-(FMath::Sin(runningTime*2) + 1) * 0.5f;
	
	FVector newPos = FMath::Lerp( startPos, endPos, t*t);
	runningTime += DeltaTime;
	
	float x = newPos.X;
	float y = LagrangePolynomial(x);
	newPos.Z = y;
	GetOwner()->SetActorLocation(newPos);


	float dist = FVector::Dist(newPos, endPos);
	//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::SanitizeFloat(dist));
	if (hitAudio!= nullptr && !hitAudio->IsPlaying()) {
		hitAudio = nullptr;
	}
	if (dist < hitClosingDist) {
		OnGroundHit();
	}
}

